$(function () {
    var offset = 2;
    var skip = 5;
    var posts = []; //Blog

    // Get all of the Blog items and store them
    (function () {
        $.ajax({
            url: "https://api.parse.com/1/classes/BlogPost",
            type: "GET",
            dataType: 'json',
            data: {
                "keys": "title,description,image,name,link,publish_date"
            },
            headers: {
                "X-Parse-Application-Id": "DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e",
                "X-Parse-REST-API-Key": "Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo",
                "Content-Type": "application/json"
            },
            success: function (data) {
                posts = data.results.sort(dateSort).filter(function (post) {
                    return isPublished(post.publish_date);
                });
                var item = posts[0];
                if (item) {
                    // Fill in data for first blog post (example)
                    var blog_post = $(".blog-wrapper:first");
                    blog_post.find(".title").html(item.title);
                    blog_post.find(".name").html(item.name);
                    blog_post.find(".date").html(formatDate(item.publish_date));
                    blog_post.find(".link").attr("href", 'moreblog/' + item.link);
                    blog_post.find(".description").html( formatDescription(item.description) );
                    blog_post.find(".image").attr("src", item.image);

                    // File in data for another blog post
                    addPost(posts[1]);
                }
            }
        });
        return false;
    })();

    // Add more projects when we reach the end of the section
    $(window).scroll(function () {
      if ($(this).scrollTop() >= $('#blog-container').height() - $(this).height()) {
            var length = Math.min(offset + skip, posts.length);

            for (offset; offset < length; offset++) {
                addPost(posts[offset]);
            }
        }
    });

    // Create a new Second Blog by cloning an existing one and changing the data
    function addPost(item) {
        if (item) {
          var blog_post = $(".blog-wrapper:first").clone();
          blog_post.find(".title").html(item.title);
          blog_post.find(".name").html("by " + item.name);
          blog_post.find(".date").html(formatDate(item.publish_date));
          blog_post.find(".link").attr("href", 'moreblog/' + item.link);
          blog_post.find(".description").html( formatDescription(item.description) );
          blog_post.find(".image").attr("src", item.image);
          blog_post.appendTo("#blog-container");
        }
    }

    // Formates the date on display
    function formatDate(date) {
        return moment(date.toString()).format('DD MMMM YYYY');
    }

    // Sort the blog items so they are in cronological order
    function dateSort(a, b) {
        var moment_a = moment(a.publish_date).valueOf();
        var moment_b = moment(b.publish_date).valueOf();
        return moment_b - moment_a;
    }

    function isPublished(time) {
        var now = moment();
        var published = moment(time);
        return (published.diff(now) <= 0);
    }

    function formatDescription(description) {
      var split = description.split("\n").filter(function(x) { return x.length > 0; });
      var result = "";
      var index = 0;
      while(result.length < 140 && split[index]) {
        result += split[index] + " ";
        index++;
      }

      return result;
    }

})();
