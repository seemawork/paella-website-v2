$(function () {

    // Scrolls to specified element
    $("a[href^='#']").on('click', function (event) {
        var target = $(this).attr("href");

        if (target.length) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 500);
        }
    });

    // Resets progress cirlce animation so it can run one
    $('.circle').each(function () {
        var circle = $(this)[0];
        circle.style.strokeDashoffset = 3000;
        $(this).parent().siblings('.figure').find(">:first-child").text(0);
    });

    // Activate progress circle animation when the element is in the view
    $(window).scroll(function () {
        $('.circle').each(function () {
            var imagePos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();
            var run = $(this).data('run');

            // Is the element in the viewport and the animation hasn't run yet
            if (imagePos < topOfWindow + $(window).height() && !run) {
                $(this).data('run', true);
                animate($(this));
            }
        });
    });

    /**
     * Starts the progress circle animation
     * @param {JQuery} el - The element being animated
     */
    function animate(el) {
        var delay = parseInt(el.data("delay"));

        // Run animations after a delay if one exists
        if (delay) {
            el.delay(delay).queue(function () {
                animateStat(el);
            });
        } else {
            animateStat(el);
        }
    }

    /**
     * Runs the progress circle animation
     * @param {JQuery} el - The element being animated
     */
    function animateStat(el) {
        var p = el[0];
        var offset = 3000;
        var run = true;

        var animateCirlce = function () {
            if (offset <= 0) {
                offset = 0;
                run = false;
            }

            p.style.strokeDashoffset = offset;
            offset -= 25;

            if (run) {
                requestAnimationFrame(animateCirlce);
            }
        };

        var incrementNumber = function () {
            var me = el.parent().siblings('.figure').find(">:first-child");
            var start = 0;
            var end = parseInt(me.data('count')) || 0;
            var increment = Math.ceil(end / 30);

            var counter = setInterval(function (el) {

                start += increment;
                me.text(start);

                if (start >= end) {
                    if (end > 1000) {
                        end = Math.floor(end / 1000) + "K+";
                    }

                    me.text(end);
                    clearInterval(counter);
                }
            }, 50);
        };

        animateCirlce();
        incrementNumber();
    }
});
