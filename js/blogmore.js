$(function () {
    var posts = []; //Blog

    // Get all of the Blog items and store them
    (function () {
        $.ajax({
            url: "https://api.parse.com/1/classes/BlogPost",
            type: "GET",
            dataType: 'json',
            data: {
                "keys": "title,description,image,link"
            },
            headers: {
                "X-Parse-Application-Id": "DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e",
                "X-Parse-REST-API-Key": "Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo",
                "Content-Type": "application/json"
            },
            success: function (data) {
                var id = $('._id').data('link');
                posts = data.results.sort(dateSort).slice(0, 3).filter(function (post) {
                    return post.objectId !== id;
                });

                var item = posts[0];
                if (item) {
                    // Fill in data for first blog post (example)
                    var blog_post = $(".side-post:first");
                    blog_post.find(".title").html(item.title);
                    blog_post.find(".link").attr("href", item.link);
                    blog_post.find(".description").html( formatDescription(item.description) );
                    blog_post.find(".image").attr("src", item.image);

                    // File in data for another blog post
                    item = posts[1];
                    if(item) {
                      // Fill in data for first blog post (example)
                      var second_post = $(".side-post:last");
                      second_post.find(".title").html(item.title);
                      second_post.find(".link").attr("href", item.link);
                      second_post.find(".description").html( formatDescription(item.description) );
                      second_post.find(".image").attr("src", item.image);
                    } else {
                      $(".side-post:last").remove();
                    }
                }
            }
        });
        return false;
    })();

    // Formates the date on display
    function formatDate(date) {
        return moment(date.toString()).format('DD MMMM YYYY');
    }

    // Sort the blog items so they are in cronological order
    function dateSort(a, b) {
        var moment_a = moment(a.publish_date).valueOf();
        var moment_b = moment(b.publish_date).valueOf();
        return moment_b - moment_a;
    }

    function isPublished(time) {
        var now = moment();
        var published = moment(time);
        return (published.diff(now) <= 0);
    }

    function formatDescription(description) {
      var split = description.split("\n").filter(function(x) { return x.length > 0; });
      var result = "";
      var index = 0;
      while(result.length < 140 && split[index]) {
        result += split[index] + " ";
        index++;
      }

      return result;
    }

});
