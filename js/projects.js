$(function () {
    var offset = 2;
    var skip = 3;
    var projects = [];

    // Get all of the projects and store them
    (function () {
        $.ajax({
            url: "https://api.parse.com/1/classes/Project",
            type: "GET",
            dataType: 'json',
            data: {
                "keys": "title,client,duration,skills,description,project_link,img_path",
                'order': '-createdAt'
            },
            headers: {
                "X-Parse-Application-Id": "DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e",
                "X-Parse-REST-API-Key": "Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo",
                "Content-Type": "application/json"
            },
            success: function (data) {
                projects = data.results;
                var item = projects[0];

                if (item) {
                    // Fill in data for first project
                    var next_project = $("#project-container > .project-wrapper:first");
                    next_project.find(".title").html(item.title);
                    next_project.find(".client").html(item.client);
                    next_project.find(".duration").html(item.duration);
                    next_project.find(".skills").html(item.skills);
                    next_project.find(".description").html(item.description);
                    next_project.find(".project_link").attr("href", item.project_link);
                    next_project.find(".img_path").attr("src", item.img_path);

                    // File in data for another project
                    addProject(projects[1]);

                    // Remove pointless static content
                    deleteStaticContent();

                }
            }
        });
        return false;
    })();

    // Add more projects when we reach the end of the section
    $(window).scroll(function () {

        if ($(this).scrollTop() >= $('#project-container').height() - $(this).height()) {
            var length = Math.min(offset + skip, projects.length);

            for (offset; offset < length; offset++) {
                addProject(projects[offset]);
            }
        }
    });

    // Create a new project by cloning an existing one and changing the data
    function addProject(item) {
        if (item) {
            var next_project = $("#project-container > .project-wrapper:first").clone();
            next_project.find(".title").html(item.title);
            next_project.find(".client").html(item.client);
            next_project.find(".duration").html(item.duration);
            next_project.find(".skills").html(item.skills);
            next_project.find(".description").html(item.description);
            next_project.find(".project_link").attr("href", item.project_link);
            next_project.find(".img_path").attr("src", item.img_path);
            next_project.appendTo("#project-container");
        }
    }

    function deleteStaticContent() {
        // Remove the static project section
        $("#project-container > .project-wrapper.nojs").remove();
    }

});
