$(function () {
    var offset = 1;
    var skip = 5;
    var history = [];

    // Get all of the history items and store them
    (function () {
        $.ajax({
            url: "https://api.parse.com/1/classes/History",
            type: "GET",
            dataType: 'json',
            data: {
                "keys": "date,icon,content"
            },
            headers: {
                "X-Parse-Application-Id": "DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e",
                "X-Parse-REST-API-Key": "Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo",
                "Content-Type": "application/json"
            },
            success: function (data) {
                history = data.results.sort(dateSort);
                var item = history[0];

                if (item) {
                    // Fill in data for first history item
                    var history_item = $("#history-timeline > .timeline-block:first");
                    history_item.find(".timeline-content > div > h5").html(formatDate(item.date));
                    history_item.find(".timeline-content > div > p").html(item.content);
                    history_item.find(".icon").addClass(item.icon);

                    // File in data for some more timeline items
                    var length = Math.min(skip, history.length);
                    for (offset; offset < length; offset++) {
                        addItem(history[offset]);
                    }

                    // Remove pointless static content
                    deleteStaticContent();

                }
            }
        });
        return false;
    })();

    // Add more history items when we reach the end of the section
    $(window).scroll(function () {

        if ($(this).scrollTop() >= $('#history-timeline').height() - $(this).height()) {
            var length = Math.min(offset + skip, history.length);

            for (offset; offset < length; offset++) {
                addItem(history[offset]);
            }
        }
    });

    // Create a new history item by cloning an existing one and changing the data
    function addItem(item) {
        if (item) {
            var history_item = $("#history-timeline > .timeline-block:first").clone();
            var classes = "icon fa fa-stack-1x fa-inverse " + item.icon;
            history_item.find(".timeline-content > div > h5").html(formatDate(item.date));
            history_item.find(".timeline-content > div > p").html(item.content);
            history_item.find(".icon").attr("class", classes);
            history_item.appendTo("#history-timeline");
        }
    }

    // Formates the date on display
    function formatDate(date) {
        var formatted = moment(date.toString()).format('DD MMMM YYYY');
        if (!moment(formatted).isValid()) {
            formatted = date.toString();
        }
        return formatted;
    }

    // Sort the history items so they are in cronological order
    function dateSort(a, b) {
        return new Date(b.date) - new Date(a.date);
    }

    function deleteStaticContent() {
        // Remove the static history items
        $("#history-timeline > .timeline-block.nojs").remove();
    }

});
