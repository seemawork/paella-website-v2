function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: {
            lat: 51.494335,
            lng: -0.061212
        },
        scrollwheel: false
    });
}
